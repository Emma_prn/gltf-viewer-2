#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

bool ViewerApplication::loadGltfFile(tinygltf::Model & model){
  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;

  std::cout << "Loading : " << m_gltfFilePath.string() << std::endl;

  bool load = loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

  if (!warn.empty()) {
    std::cerr << warn << std::endl;
  }

  if (!err.empty()) {
    std::cerr << err << std::endl;
  }

  if (!load) {
    std::cerr << "Failed to parse glTF" << std::endl;
    return false;
  }

  return true;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const {
  std::vector<GLuint> textureObjects(model.textures.size(), 0);
  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);
  glGenTextures(GLsizei(model.textures.size()), textureObjects.data());
  for (size_t i = 0; i < model.textures.size(); i++)
  {
    const auto &texture = model.textures[i];
    assert(texture.source >= 0);
    const auto &image = model.images[texture.source];
    const auto &sampler =
        texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
    glBindTexture(GL_TEXTURE_2D, textureObjects[i]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
        GL_RGBA, image.pixel_type, image.image.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
        sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
        sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
      sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
      sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
      sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }
  }
  glBindTexture(GL_TEXTURE_2D, 0);  
  return textureObjects;
}

std::vector<GLuint> ViewerApplication::createBufferObjects(const tinygltf::Model &model){
  std::vector<GLuint> bufferObjects(model.buffers.size(), 0);

  glGenBuffers(GLsizei(model.buffers.size()), bufferObjects.data());
  for (size_t i = 0; i < model.buffers.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER,  model.buffers[i].data.size(),
        model.buffers[i].data.data(), 0);
  }
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return bufferObjects;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects( const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects, std::vector<VaoRange> & meshIndexToVaoRange){
  std::vector<GLuint> vertexArrayObjects;

  meshIndexToVaoRange.resize(model.meshes.size());

  const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
  const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
  const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

  for (size_t i = 0; i < model.meshes.size(); i++)
  {
    const auto &mesh = model.meshes[i];
    auto vaoRange = meshIndexToVaoRange[i];

    vaoRange.begin = GLsizei(vertexArrayObjects.size());
    vaoRange.count = GLsizei(mesh.primitives.size());
    vertexArrayObjects.resize(vertexArrayObjects.size() + mesh.primitives.size());

    glGenVertexArrays(vaoRange.count, &vertexArrayObjects[vaoRange.begin]);

    for (size_t idX = 0; idX < mesh.primitives.size(); idX++)
    {
      const auto vao = vertexArrayObjects[vaoRange.begin + idX];
      const auto &primitive = mesh.primitives[idX]; 
      glBindVertexArray(vao);

      { // Position Attribute
        const auto iterator = primitive.attributes.find("POSITION");
        if (iterator != end(primitive.attributes)) { 
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          const auto bufferObject = bufferObjects[bufferIdx];

          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), (const GLvoid *)byteOffset);
        }
      }

      { // Normal Attribute
        const auto iterator = primitive.attributes.find("NORMAL");
        if (iterator != end(primitive.attributes)) { 
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          const auto bufferObject = bufferObjects[bufferIdx];

          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), (const GLvoid *)byteOffset);
        }
      }

      { // Texcoord 0 Attribute
        const auto iterator = primitive.attributes.find("TEXCOORD_0");
        if (iterator != end(primitive.attributes)) { 
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          const auto bufferObject = bufferObjects[bufferIdx];

          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), (const GLvoid *)byteOffset);
        }
      }

      if (primitive.indices >= 0)
      {
        const auto accessorIdx = primitive.indices;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
      }
      
    }
    
  }
  glBindVertexArray(0);
  std::cout << "Number of VAOs : " << vertexArrayObjects.size() << std::endl;

  return vertexArrayObjects;
}

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram =
      compileProgram({m_ShadersRootPath / m_vertexShader,
          m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");

  const auto uLightDirectionLocation = glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  const auto uLightIntensityLocation = glGetUniformLocation(glslProgram.glId(), "uLightIntensity");
  const auto uBaseColorTextureLocation = glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  const auto uBaseColorFactorLocation = glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");
  
  const auto uMetallicRoughnessTextureLocation = glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
  const auto uMetallicFactorLocation = glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto uRoughnessFactorLocation = glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");

  const auto uEmissiveTextureLocation = glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
  const auto uEmissiveFactorLocation = glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");

  const auto uOcclusionTextureLocation = glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto uOcclusionStrengthLocation = glGetUniformLocation(glslProgram.glId(), "uOcclusionStrenght");
  const auto uApplyOcclusionLocation = glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");

  tinygltf::Model model;
  if (!loadGltfFile(model))
  {
    return -1;
  }

  glm::vec3 bboxMax, bboxMin;
  computeSceneBounds(model,bboxMin,bboxMax);

  // Build projection matrix
  const auto diagonal = bboxMax - bboxMin;
  auto maxDist = glm::length(diagonal);
  maxDist = maxDist != 0 ? maxDist : 100;
  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDist, 1.5f * maxDist);

  // TODO Implement a new CameraController model and use it instead. Propose the
  // choice from the GUI
  std::unique_ptr<CameraController> cameraController = std::make_unique<TrackballCameraController>(
      m_GLFWHandle.window(), 0.5f * maxDist);
  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {
    const auto center = 0.5f * (bboxMax + bboxMin);
    const auto up = glm::vec3(0,1,0);
    const auto eye = diagonal.z > 0 ?
      diagonal + center : center + 2.f * glm::cross(diagonal,up);
    cameraController->setCamera(Camera{eye, center, up});
  }

  std::vector<GLuint> bufferObjects;
  bufferObjects = createBufferObjects(model);

  const auto textureObjects = createTextureObjects(model);

  GLuint Oritexture = 0;
  glGenTextures(1, &Oritexture);
  glBindTexture(GL_TEXTURE_2D, Oritexture);
  float white[] = {1,1,1,1};
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
  glBindTexture(GL_TEXTURE_2D, 0);

  glm::vec3 lightDirection(1,1,1);
  glm::vec3 lightIntensity(1,1,1);
  bool lightFromCam = false;
  bool applyOcclusion = true;

  // TODO Creation of Vertex Array Objects
  std::vector<VaoRange> meshToVertexArray;
  std::vector<GLuint> vertexArray;
  vertexArray = createVertexArrayObjects(model, bufferObjects,meshToVertexArray);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  const auto bindMaterial = [&](const auto materialIndex) {
    if (materialIndex >= 0)
    {
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;

      if (uBaseColorFactorLocation >= 0)
      {
        glUniform4f(uBaseColorFactorLocation,
            (float)pbrMetallicRoughness.baseColorFactor[0],
            (float)pbrMetallicRoughness.baseColorFactor[1],
            (float)pbrMetallicRoughness.baseColorFactor[2],
            (float)pbrMetallicRoughness.baseColorFactor[3]);
      }

      if (uBaseColorTextureLocation >= 0)
      {
        auto textureObj = Oritexture;
        if (pbrMetallicRoughness.baseColorTexture.index >= 0)
        {
          const auto &texture =
              model.textures[pbrMetallicRoughness.baseColorTexture.index];
          if (texture.source >= 0)
          {
            textureObj = textureObjects[texture.source];
          }
        }
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObj);
        glUniform1i(uBaseColorTextureLocation, 0);
      }

      if (uMetallicFactorLocation >= 0)
      {
        glUniform1f(uMetallicFactorLocation, (float)pbrMetallicRoughness.metallicFactor);
      }

      if (uRoughnessFactorLocation >= 0)
      {
        glUniform1f(uRoughnessFactorLocation, (float)pbrMetallicRoughness.roughnessFactor);
      }

      if (uMetallicRoughnessTextureLocation >= 0)
      {
        auto textureObj = 0u;
        if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
          const auto &texture = model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];
          if (texture.source >= 0) {
            textureObj = textureObjects[texture.source];
          }
        }
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureObj);
        glUniform1i(uMetallicRoughnessTextureLocation, 1);
      }

      if (uEmissiveFactorLocation >= 0)
      {
        glUniform3f(uEmissiveFactorLocation, (float)material.emissiveFactor[0], (float)material.emissiveFactor[1],
        (float)material.emissiveFactor[2]);
      }

      if (uEmissiveTextureLocation >= 0)
      {
        auto textureObj = 0u;
        if (material.emissiveTexture.index >= 0)
        {
          const auto &texture = model.textures[material.emissiveTexture.index];
          if (texture.source >= 0) {
            textureObj = textureObjects[texture.source];
          }
        }
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textureObj);
        glUniform1i(uEmissiveTextureLocation, 2);
      }
      
      if (uOcclusionStrengthLocation >= 0)
      {
        glUniform1f(uOcclusionStrengthLocation, (float)material.occlusionTexture.strength);
      }
      
      if (uOcclusionTextureLocation >= 0)
      {
        auto textureObj = Oritexture;
        if (material.occlusionTexture.index >= 0) {
          const auto &texture = model.textures[material.occlusionTexture.index];
          if (texture.source >= 0) {
            textureObj = textureObjects[texture.source];
          }
        }
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textureObj);
        glUniform1i(uOcclusionTextureLocation, 3);
      }
    }
    else {
      if (uBaseColorFactorLocation >= 0) {
        glUniform4f(uBaseColorFactorLocation, 1, 1, 1, 1);
      }
      if (uBaseColorTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, Oritexture);
        glUniform1i(uBaseColorTextureLocation, 0);
      }
      if (uMetallicFactorLocation >= 0)
      {
        glUniform1f(uMetallicFactorLocation, 1.f);
      }
      if (uRoughnessFactorLocation >= 0) {
        glUniform1f(uRoughnessFactorLocation, 1.f);
      }
      if (uMetallicRoughnessTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uMetallicRoughnessTextureLocation, 1);
      }
      if (uEmissiveFactorLocation >= 0)
      {
        glUniform3f(uEmissiveFactorLocation, 0.f, 0.f, 0.f);
      }
      if (uEmissiveTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uEmissiveTextureLocation, 2);
      }
      if (uOcclusionStrengthLocation >= 0)
      {
        glUniform1f(uOcclusionStrengthLocation, 0.f);
      }
      if (uOcclusionTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uOcclusionTextureLocation, 3);
      }
    }
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();

    if(uLightDirectionLocation >= 0){
      if (lightFromCam)
      {
        glUniform3f(uLightDirectionLocation, 0, 0, 1);
      }
      else {
        const auto lightDirectionInViewSpace = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));
        glUniform3f(uLightDirectionLocation, lightDirectionInViewSpace[0],lightDirectionInViewSpace[1],lightDirectionInViewSpace[2]);
      }
    }

    if (uLightIntensityLocation >= 0)
    {
      glUniform3f(uLightIntensityLocation, lightIntensity[0], lightIntensity[1], lightIntensity[2]);
    }
    
    if (uApplyOcclusionLocation >= 0)
    {
      glUniform1i(uApplyOcclusionLocation, applyOcclusion);
    }

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          const auto &node = model.nodes[nodeIdx];
          glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

          if (node.mesh >= 0)
          {
            auto mvMatrix = viewMatrix * modelMatrix;
            auto mvpMatrix = projMatrix * mvMatrix;
            auto normalMatrix = glm::transpose(glm::inverse(mvMatrix));

            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(mvpMatrix));
            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(mvMatrix));
             glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));

             auto mesh = model.meshes[node.mesh];
             auto vaoRange = meshToVertexArray[node.mesh];

             for (size_t pIdx = 0; pIdx < mesh.primitives.size(); pIdx++)
             {
              auto vao = vertexArray[vaoRange.begin + pIdx];
              auto primitive = mesh.primitives[pIdx];
              bindMaterial(primitive.material);

              glBindVertexArray(vao);

              if (primitive.indices >= 0)
              {
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
                glDrawElements(primitive.mode, GLsizei(accessor.count), accessor.componentType, (const GLvoid *) byteOffset);
              }
              else {
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];
                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }

              for ( auto child : node.children)
              {
                drawNode(child, modelMatrix);
              }
              
             }
          }
          
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      for (auto node : model.scenes[model.defaultScene].nodes)
      {
        drawNode(node,glm::mat4(1));
      }
      
    }
  };

  if (!m_OutputPath.empty())
  {
    const auto numComponents = 3;
    std::vector<unsigned char> pixels(m_nWindowWidth * m_nWindowHeight * numComponents);
    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), [&]() {
      drawScene(cameraController->getCamera());
    });

    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, numComponents, pixels.data());
    const auto strPath = m_OutputPath.string();
    stbi_write_png(
      strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);
    return 0;
  }
  

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }
        static int cameraType = 0;
        auto cameraTypeChange = ImGui::RadioButton("Trackball", &cameraType, 0) || ImGui::RadioButton("First Person", &cameraType, 1);
        if (cameraTypeChange)
        {
          auto currentCamera = cameraController->getCamera();
          if (cameraType == 0)
          {
            cameraController = std::make_unique<TrackballCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDist);

          }
          else {
            cameraController = std::make_unique<FirstPersonCameraController>(m_GLFWHandle.window(), 0.5f * maxDist);
          }
          cameraController->setCamera(currentCamera);
        }
         
      }
      if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen))
      {
        static float theta = 0.f;
        static float phi = 0.f;

        if (ImGui::SliderFloat("Theta", &theta, 0, glm::pi<float>()) || ImGui::SliderFloat("Phi", &phi, 0, 2.f * glm::pi<float>()))
        {
          auto sinPhi = glm::sin(phi);
          auto cosPhi = glm::cos(phi);
          auto sinTheta = glm::sin(theta);
          auto cosTheta = glm::cos(theta);
          lightDirection = glm::vec3(sinTheta * cosPhi, cosTheta, sinTheta * sinPhi);
        }
        
        static glm::vec3 lightColor(1.f, 1.f, 1.f);
        static float lightIntensityFactor = 1.f;

        if (ImGui::ColorEdit3("Color", (float*)&lightColor) || ImGui::InputFloat("Intensity", &lightIntensityFactor))
        {
          lightIntensity = lightColor * lightIntensityFactor;
        }
        ImGui::Checkbox("Light from camera", &lightFromCam);
        ImGui::Checkbox("Apply Occlusion", &applyOcclusion);
      }
      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}
